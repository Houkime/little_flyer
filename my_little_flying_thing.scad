module propeller(num=3, thickness=1, r=10,shaft_r=5,height=5,hole_r=3,twist=90,slices=20){
    
    union(){
    intersection(){
    
    linear_extrude(height=height,twist=twist,slices=slices, center = true) petals_profile(num, thickness, r, hole_r);
    
    scale([1,1,height/(r-hole_r)]) ring((r-hole_r)/2+hole_r,(r-hole_r)/2);
    }
    
    difference(){
        cylinder(h=height,r=shaft_r,center=true);
        cylinder(h=height*1.1,r=hole_r,center=true);
        }
    }
    }
    
module ring(r1=1,r2=0.3, arc=360){
    assert(r1>r2);
    rotate_extrude(angle=arc) translate([r1,0,0]) circle(r2);
    }
   
  //  rotate_extrude() circle(10);
    
module ring_of(radius, count)
{
    for (a = [0 : count - 1]) {
        angle = a * 360 / count;
        translate(radius * [sin(angle), -cos(angle), 0])
            rotate([0, 0, angle])
                children();
    }
}
    
module petals_profile(num=3, thickness=1, r=10, hole_r=1){
    //union()
        ring_of((r-hole_r)/2+hole_r,num)
            square([thickness,r-hole_r],center=true);
        
} 

module line_hull(){
    echo($children, " children");
    union(){
    for (a = [ 0 : 1 : $children-2 ])
        hull() {
            children(a);
            children(a+1);
        }
    }
    }

function QB(q,p0,p1,p2) = pow(1-q,2)*p0+2*(1-q)*q*p1+q*q*p2;
//cubic besier line
function CB(q,p0,p1,p2,p3) = pow(1-q,3)*p0+3*pow(1-q,2)*q*p1+3*(1-q)*q*q*p2+q*q*q*p3;

module insect_bezier_line(steps,p0,p1,p2,r1,r2){
step=1/steps;
   union()
   for (a = [ 0 : step : 1-step ])
   {
       hull(){
           translate(QB(a,p0,p1,p2)) sphere(r1+(r2-r1)*a);
           translate(QB(a+step,p0,p1,p2)) sphere(r1);
           } 
       
   }

}

module bezier_line(steps,p0,p1,p2,r1,r2){
step=1/steps;
   union()
   for (a = [ 0 : step : 1-step ])
   {
       hull(){
           translate(QB(a,p0,p1,p2)) sphere(r1+(r2-r1)*a);
           translate(QB(a+step,p0,p1,p2)) sphere(r1+(r2-r1)*(a+step));
           } 
       
   }

}

module cubic_bezier_line(steps,p0,p1,p2,p3,r1,r2){
step=1/steps;
   union()
   for (a = [ 0 : step : 1-step ])
   {
       hull(){
           translate(CB(a,p0,p1,p2,p3)) sphere(r1+(r2-r1)*a);
           translate(CB(a+step,p0,p1,p2,p3)) sphere(r1+(r2-r1)*(a+step));
           } 
       
   }

}
    

fan_position=[80,50,-60];
fan_radius = 15;

module arm(root_radius=4){

fan_clearance = 1;
fan_ring_small_r = 2.5;
fan_attachment_offset = fan_radius+fan_clearance+fan_ring_small_r;
fan_attachment_position = fan_position + [-fan_attachment_offset,0,0];

guard_length = 20;

steps=10;
origin = [0,0,0];
#color("blue") translate(origin) sphere(5);
dest = fan_attachment_position;
#color("red") translate(dest) sphere(2);
middle = [(origin.x+dest.x)/2,dest.y+(dest.y-origin.y)*0.5,(origin.z+dest.z)/2];
#color("yellow") translate(middle) sphere(5);
fan_entrance_guard=fan_attachment_position-[guard_length,0,0];
#color("magenta") translate(fan_entrance_guard) sphere(2);

cubic_bezier_line(steps,origin,middle,fan_entrance_guard,dest,root_radius,fan_ring_small_r);

translate(fan_position) ring(fan_attachment_offset,fan_ring_small_r);

translate(fan_position)
    %propeller(num=3, thickness=1, r=fan_radius,shaft_r=fan_radius/2,height=5,hole_r=fan_radius/4,twist=90,slices=20);
}

module flattop_sphere(radius,fraction_visible){
    
    intersection(){
        translate([0,0,-radius]) cube(radius*2, center=true);
        translate([0,0,-(fraction_visible-0.5)*radius*2])sphere(radius);
    }
    
}

root_radius=6;
translate([0,0,-root_radius])
    union(){
        arm(root_radius);
        mirror([0,1,0]) arm(root_radius);        
        }
flattop_sphere(30,0.4);
deck_radius = 50;
deck_thickness=3;
ornamental_ring_thickness=4;
difference(){
    linear_extrude(deck_thickness,center=true) circle(deck_radius);
    translate([0,0,deck_thickness]){
        union(){
            ring(deck_radius*0.8,ornamental_ring_thickness/2);
            ring(deck_radius*0.6,ornamental_ring_thickness/2);  
        }      
    }
}

vbar_r=1;
hbar_r=1;
top_hbar_r=2;
fence_height=10;
hbar_num=2;
vbar_num=8;
vbar_arc=60;

fence_curvature = fence_height/sin(vbar_arc);
fence_radius = deck_radius * 0.9;


translate([0,0,deck_thickness/2]){
    ring_of(fence_radius,vbar_num)
    rotate([90,0,90])
    translate([-fence_curvature,0,0])
    ring(fence_curvature,vbar_r,vbar_arc);
}

function hbar_big_radius(q) // q from 0 to 1
    = fence_radius+(1-cos(q*vbar_arc))*fence_curvature;

function hbar_height(q) // q from 0 to 1
    = sin(vbar_arc*q)*fence_curvature;

module hbar(q,r){
    translate([0,0,hbar_height(q)]) ring(hbar_big_radius(q),r);
    }

module hbars(r,top_r,num){
    union()
    for (q=[0:1/num:1]) {
        if (q==1) hbar(q,top_r);
        else hbar(q,r);
    }
    }

hbars(hbar_r,top_hbar_r,hbar_num);

*translate(fan_position) propeller(num=3, thickness=1, r=fan_radius,shaft_r=fan_radius/2,height=5,hole_r=fan_radius/4,twist=90,slices=20);
    